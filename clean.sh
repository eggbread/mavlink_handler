#!/bin/bash
################################################################################
# Copyright (c) 2020 ModalAI, Inc. All rights reserved.
################################################################################

sudo rm -rf build/
sudo rm -rf ipk/data/
sudo rm -rf ipk/control.tar.gz
sudo rm -rf ipk/data.tar.gz
sudo rm -rf mavlink-camera-manager*.ipk
sudo rm -rf .bash_history

echo ""
echo "Done cleaning"
echo ""
