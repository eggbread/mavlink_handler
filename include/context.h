/**
 * @file context.h
 *
 * This file contains the definition of the main data structure for the
 * application.
 */

#ifndef CONTEXT_H
#define CONTEXT_H

#include <stdint.h>
#include <pthread.h>

#define PROGRAM_ERROR -1

#define QGC_IP_ADDRESS_MAX_SIZE 64

// Per Mavlink video stream information message
#define RTSP_URI_MAX_SIZE 160

// Per Mavlink camera information message
#define CAMERA_NAME_MAX_SIZE 32

// Maximum camera component ids available in Mavlink
#define MAX_MAVLINK_CAMERAS 6

// Structure to contain all needed information, so we can pass it to callbacks
typedef struct _context_data {

    char qgc_ip_address[QGC_IP_ADDRESS_MAX_SIZE];
    char rtsp_server_uri[RTSP_URI_MAX_SIZE];
    char camera_name[CAMERA_NAME_MAX_SIZE];

    uint8_t camera_id;
    uint8_t camera_component_id;

    uint8_t debug;
    uint8_t frame_debug;

    volatile uint8_t running;

    pthread_mutex_t lock;

} context_data;

#endif // CONTEXT_H
