#ifndef HANDLER_H
#define HANDLER_H

#include "context.h"

/**
 * @brief      Initialize the QGC module and start helper threads
 *
 * @param[in]  ctx     A pointer to the application context data structure
 *
 * @return     0 on success, -1 on failure
 */
int qgc_init(context_data *ctx);

/**
 * @brief      Shutdown the QGC module
 */
void qgc_shutdown();

#endif /* HANDLER_H */
