#!/bin/bash
################################################################################
# Copyright (c) 2020 ModalAI, Inc. All rights reserved.
################################################################################

mavlink_ipk=mavlink_v2.0_8x96.ipk

if [ ! -f $mavlink_ipk ]; then
    wget https://storage.googleapis.com/modalai_public/modal_packages/archive/$mavlink_ipk
fi

sudo opkg install $mavlink_ipk

exit 0
