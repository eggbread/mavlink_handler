#include <stdio.h>
#include <stdint.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <unistd.h>
#include <time.h>

#include <mavlink/v2.0/common/mavlink.h>

#include "qgc.h"

#define MAX_MESSAGE_LENGTH 256
#define UDP_READ_BUF_LEN 512

// TODO: Should the system ID be configurable?
static int mavlink_system_id = 1;

// Pointer to the application data
static context_data *context = NULL;

// Socket definitions
static int qgc_sockfd;
static int px4_sockfd;
static struct sockaddr_in qgc_servaddr;
static struct sockaddr_in px4_servaddr;

// Thread definitions
static pthread_t qgc_thread_id;
static pthread_t px4_thread_id;

// Mavlink message definitions
static unsigned char ping_send_buffer[MAX_MESSAGE_LENGTH];
static unsigned int ping_message_size = 0;
static unsigned char camera_heartbeat_send_buffer[MAX_MESSAGE_LENGTH];
static unsigned int camera_heartbeat_message_size = 0;
static unsigned char command_ack_send_buffer[MAX_MESSAGE_LENGTH];
static unsigned int command_ack_message_size = 0;
static unsigned char camera_information_send_buffer[MAX_MESSAGE_LENGTH];
static unsigned int camera_information_message_size = 0;
static unsigned char camera_settings_send_buffer[MAX_MESSAGE_LENGTH];
static unsigned int camera_settings_message_size = 0;
static unsigned char video_stream_information_send_buffer[MAX_MESSAGE_LENGTH];
static unsigned int video_stream_information_message_size = 0;
static unsigned char video_stream_status_send_buffer[MAX_MESSAGE_LENGTH];
static unsigned int video_stream_status_message_size = 0;
static unsigned char storage_information_send_buffer[MAX_MESSAGE_LENGTH];
static unsigned int storage_information_message_size = 0;
static unsigned char camera_capture_status_send_buffer[MAX_MESSAGE_LENGTH];
static unsigned int camera_capture_status_message_size = 0;

static uint32_t get_ms_since_boot() {
    struct timespec tp;
    int rc = clock_gettime(CLOCK_MONOTONIC, &tp);
    uint32_t ms_since_boot = ((tp.tv_sec * 1000) + (tp.tv_nsec / 1000000));
    if (context->debug) printf("Milliseconds since boot %u ms\n", ms_since_boot);
    return ms_since_boot;
}

static void send_mavlink_command_ack(mavlink_message_t* msg) {
    mavlink_message_t send_message;

    if (context->debug) printf("Sending command ACK\n");

    mavlink_msg_command_ack_pack(mavlink_system_id, context->camera_component_id,
                                 &send_message,
                                 MAV_CMD_REQUEST_STORAGE_INFORMATION,
                                 MAV_RESULT_ACCEPTED,
                                 0,
                                 0,
                                 msg->sysid, msg->compid);
    command_ack_message_size = mavlink_msg_to_send_buffer(command_ack_send_buffer,
                                                          (const mavlink_message_t *) &send_message);
    sendto(qgc_sockfd, command_ack_send_buffer, \
           command_ack_message_size, MSG_CONFIRM, \
           (const struct sockaddr *) &qgc_servaddr, sizeof(qgc_servaddr));
}

static void qgc_message_handler(mavlink_message_t* msg) {
    mavlink_message_t send_message;

    if (context->debug) printf("Received msg: ID: %d sys:%d comp:%d\n", msg->msgid, msg->sysid, msg->compid);

    switch (msg->msgid)
    {
    case MAVLINK_MSG_ID_HEARTBEAT:
        if (context->debug) printf("Heartbeat message at camera\n");
        break;
    case MAVLINK_MSG_ID_PING:
        {
            mavlink_ping_t *ping_data = (mavlink_ping_t*) msg->payload64;
            if ((ping_data->target_system == 0) && (ping_data->target_component == 0)) {
                printf("Got a Ping Request from system %d, component %d\n", msg->sysid, msg->compid);
                mavlink_msg_ping_pack(mavlink_system_id, context->camera_component_id,
                                      &send_message,
                                      ping_data->time_usec,
                                      ping_data->seq,
                                      msg->sysid,
                                      msg->compid);
                ping_message_size = mavlink_msg_to_send_buffer(ping_send_buffer,
                                                               (const mavlink_message_t *) &send_message);
                sendto(qgc_sockfd, ping_send_buffer, \
                       ping_message_size, MSG_CONFIRM, \
                       (const struct sockaddr *) &qgc_servaddr, sizeof(qgc_servaddr));
            } else if (context->debug) {
                printf("Got a Ping Response\n");
                printf("\ttime_usec: %llu, seq: %u, target_system: %d, target_component: %d\n",
                       ping_data->time_usec, ping_data->seq, ping_data->target_system,
                       ping_data->target_component);
            }
        }
        break;
    case MAVLINK_MSG_ID_COMMAND_LONG:
        {
            if (context->debug) printf("Command long message at camera\n");

            mavlink_command_long_t *command_long_data =
                                    (mavlink_command_long_t*) msg->payload64;
            if (command_long_data->target_system != mavlink_system_id) {
                fprintf(stderr, "WARNING: Command system ID is not a match\n");
                break;
            }
            if (command_long_data->target_component != context->camera_component_id) {
                fprintf(stderr, "WARNING: Command component ID is not a match\n");
                break;
            }
            if (command_long_data->command == MAV_CMD_REQUEST_CAMERA_INFORMATION) {
                if (context->debug) {
                    printf("Got camera information request\n");

                    if (((int) command_long_data->param1) != 1) {
                        printf("...not requesting capabilities???...\n");
                    }
                }

                // Send the Command ACK
                send_mavlink_command_ack(msg);

                // Send the camera information
                /* @param time_boot_ms [ms] Timestamp (time since system boot).
                 * @param vendor_name  Name of the camera vendor
                 * @param model_name  Name of the camera model
                 * @param firmware_version  Version of the camera firmware (v << 24 & 0xff = Dev, v << 16 & 0xff = Patch, v << 8 & 0xff = Minor, v & 0xff = Major)
                 * @param focal_length [mm] Focal length
                 * @param sensor_size_h [mm] Image sensor size horizontal
                 * @param sensor_size_v [mm] Image sensor size vertical
                 * @param resolution_h [pix] Horizontal image resolution
                 * @param resolution_v [pix] Vertical image resolution
                 * @param lens_id  Reserved for a lens ID
                 * @param flags  Bitmap of camera capability flags.
                 * @param cam_definition_version  Camera definition version (iteration)
                 * @param cam_definition_uri  Camera definition URI (if any, otherwise only basic functions will be available).
                 *                            HTTP- (http://) and MAVLink FTP- (mavlinkftp://) formatted URIs are allowed (and
                 *                            both must be supported by any GCS that implements the Camera Protocol).
                 */
                mavlink_msg_camera_information_pack(mavlink_system_id, context->camera_component_id,
                                                    &send_message,
                                                    get_ms_since_boot(),
                                                    (const uint8_t *) "Unknown",
                                                    (const uint8_t *) context->camera_name,
                                                    1 | (218 << 8),
                                                    100.0,
                                                    10.0,
                                                    10.0,
                                                    640,
                                                    480,
                                                    0,
                                                    CAMERA_CAP_FLAGS_HAS_VIDEO_STREAM,
                                                    0,
                                                    NULL);

                camera_information_message_size = mavlink_msg_to_send_buffer(camera_information_send_buffer,
                                                                             (const mavlink_message_t *) &send_message);
                sendto(qgc_sockfd, camera_information_send_buffer, \
                       camera_information_message_size, MSG_CONFIRM, \
                       (const struct sockaddr *) &qgc_servaddr, sizeof(qgc_servaddr));

            } else if (command_long_data->command == MAV_CMD_REQUEST_CAMERA_SETTINGS) {
                if (context->debug) {
                    printf("Got camera settings request\n");

                    if (((int) command_long_data->param1) != 1) {
                        printf("...not requesting settings???...\n");
                    }
                }
                // Send the Command ACK
                send_mavlink_command_ack(msg);

                // Send the camera settings
                /* @param time_boot_ms [ms] Timestamp (time since system boot).
                 * @param mode_id  Camera mode
                 * @param zoomLevel  Current zoom level (0.0 to 100.0, NaN if not known)
                 * @param focusLevel  Current focus level (0.0 to 100.0, NaN if not known)
                 */
                mavlink_msg_camera_settings_pack(mavlink_system_id, context->camera_component_id,
                                                 &send_message,
                                                 get_ms_since_boot(),
                                                 CAMERA_MODE_VIDEO,
                                                 NAN,
                                                 NAN);
                camera_settings_message_size = mavlink_msg_to_send_buffer(camera_settings_send_buffer,
                                                                          (const mavlink_message_t *) &send_message);
                sendto(qgc_sockfd, camera_settings_send_buffer, \
                       camera_settings_message_size, MSG_CONFIRM, \
                       (const struct sockaddr *) &qgc_servaddr, sizeof(qgc_servaddr));

            } else if (command_long_data->command == MAV_CMD_REQUEST_VIDEO_STREAM_INFORMATION) {
                if (context->debug) {
                    printf("Got video stream information request for stream: %d\n",
                           (int) command_long_data->param1);
                }

                // Send the Command ACK
                send_mavlink_command_ack(msg);

                // Send the video stream information
                /* @param stream_id  Video Stream ID (1 for first, 2 for second, etc.)
                 * @param count  Number of streams available.
                 * @param type  Type of stream.
                 * @param flags  Bitmap of stream status flags.
                 * @param framerate [Hz] Frame rate.
                 * @param resolution_h [pix] Horizontal resolution.
                 * @param resolution_v [pix] Vertical resolution.
                 * @param bitrate [bits/s] Bit rate.
                 * @param rotation [deg] Video image rotation clockwise.
                 * @param hfov [deg] Horizontal Field of view.
                 * @param name  Stream name.
                 * @param uri  Video stream URI (TCP or RTSP URI ground station should connect to) or port number (UDP port ground station should listen to).
                 */
                mavlink_msg_video_stream_information_pack(mavlink_system_id, context->camera_component_id,
                                                          &send_message,
                                                          1,
                                                          1,
                                                          VIDEO_STREAM_TYPE_RTSP,
                                                          VIDEO_STREAM_STATUS_FLAGS_RUNNING,
                                                          15.0,
                                                          640,
                                                          480,
                                                          1000000,
                                                          0,  // QGC does NOT rotate the image
                                                          60,
                                                          "cam-stream",
                                                          context->rtsp_server_uri);
                video_stream_information_message_size = mavlink_msg_to_send_buffer(video_stream_information_send_buffer,
                                                                                   (const mavlink_message_t *) &send_message);
                sendto(qgc_sockfd, video_stream_information_send_buffer, \
                       video_stream_information_message_size, MSG_CONFIRM, \
                       (const struct sockaddr *) &qgc_servaddr, sizeof(qgc_servaddr));

            } else if (command_long_data->command == MAV_CMD_REQUEST_VIDEO_STREAM_STATUS) {
                if (context->debug) {
                    printf("Got video stream status request for stream: %d\n",
                           (int) command_long_data->param1);
                }

                // Send the Command ACK
                send_mavlink_command_ack(msg);

                // Send the video stream information
                /* @param stream_id  Video Stream ID (1 for first, 2 for second, etc.)
                 * @param flags  Bitmap of stream status flags
                 * @param framerate [Hz] Frame rate
                 * @param resolution_h [pix] Horizontal resolution
                 * @param resolution_v [pix] Vertical resolution
                 * @param bitrate [bits/s] Bit rate
                 * @param rotation [deg] Video image rotation clockwise
                 * @param hfov [deg] Horizontal Field of view
                 */
                mavlink_msg_video_stream_status_pack(mavlink_system_id, context->camera_component_id,
                                                     &send_message,
                                                     1,
                                                     VIDEO_STREAM_STATUS_FLAGS_RUNNING,
                                                     15.0,
                                                     640,
                                                     480,
                                                     1000000,
                                                     0,  // QGC does NOT rotate the image
                                                     60);
                video_stream_status_message_size = mavlink_msg_to_send_buffer(video_stream_status_send_buffer,
                                                                              (const mavlink_message_t *) &send_message);
                sendto(qgc_sockfd, video_stream_status_send_buffer, \
                      video_stream_status_message_size, MSG_CONFIRM, \
                      (const struct sockaddr *) &qgc_servaddr, sizeof(qgc_servaddr));

            } else if (command_long_data->command == MAV_CMD_REQUEST_STORAGE_INFORMATION) {
                if (context->debug) {
                    printf("Got storage information request. Storage ID: %d\n",
                           (int) command_long_data->param1);

                    if (((int) command_long_data->param2) != 1) {
                        printf("...not requesting information???...\n");
                    }
                }

                // Send the Command ACK
                send_mavlink_command_ack(msg);

                // Send the storage information
                /* @param time_boot_ms [ms] Timestamp (time since system boot).
                 * @param storage_id  Storage ID (1 for first, 2 for second, etc.)
                 * @param storage_count  Number of storage devices
                 * @param status  Status of storage
                 * @param total_capacity [MiB] Total capacity. If storage is not ready (STORAGE_STATUS_READY) value will be ignored.
                 * @param used_capacity [MiB] Used capacity. If storage is not ready (STORAGE_STATUS_READY) value will be ignored.
                 * @param available_capacity [MiB] Available storage capacity. If storage is not ready (STORAGE_STATUS_READY) value will be ignored.
                 * @param read_speed [MiB/s] Read speed.
                 * @param write_speed [MiB/s] Write speed.
                 */
                mavlink_msg_storage_information_pack(mavlink_system_id, context->camera_component_id,
                                                     &send_message,
                                                     get_ms_since_boot(),
                                                     1,
                                                     1,
                                                     3,   // STORAGE_STATUS_NOT_SUPPORTED
                                                     0.0,
                                                     100.0,
                                                     0.0,
                                                     0.0,
                                                     0.0);
                storage_information_message_size = mavlink_msg_to_send_buffer(storage_information_send_buffer,
                                                                              (const mavlink_message_t *) &send_message);
                sendto(qgc_sockfd, storage_information_send_buffer, \
                       storage_information_message_size, MSG_CONFIRM, \
                       (const struct sockaddr *) &qgc_servaddr, sizeof(qgc_servaddr));
            } else if (command_long_data->command == MAV_CMD_REQUEST_CAMERA_CAPTURE_STATUS) {
                if (context->debug) {
                    printf("Got camera capture status request\n");

                    if (((int) command_long_data->param1) != 1) {
                        printf("...not requesting status???...\n");
                    }
                }

                // Send the Command ACK
                send_mavlink_command_ack(msg);

                // Send the camera capture status
                /* @param time_boot_ms [ms] Timestamp (time since system boot).
                 * @param image_status  Current status of image capturing (0: idle, 1: capture in progress, 2: interval set but idle, 3: interval set and capture in progress)
                 * @param video_status  Current status of video capturing (0: idle, 1: capture in progress)
                 * @param image_interval [s] Image capture interval
                 * @param recording_time_ms [ms] Time since recording started
                 * @param available_capacity [MiB] Available storage capacity.
                 */
                mavlink_msg_camera_capture_status_pack(mavlink_system_id, context->camera_component_id,
                                                       &send_message,
                                                       get_ms_since_boot(),
                                                       0,
                                                       0,
                                                       0.0,
                                                       0,
                                                       0.0);
                camera_capture_status_message_size = mavlink_msg_to_send_buffer(camera_capture_status_send_buffer,
                                                                                (const mavlink_message_t *) &send_message);
                sendto(qgc_sockfd, camera_capture_status_send_buffer, \
                       camera_capture_status_message_size, MSG_CONFIRM, \
                       (const struct sockaddr *) &qgc_servaddr, sizeof(qgc_servaddr));
            } else {
                if (context->debug) printf("Got unknown command %d\n", command_long_data->command);
                break;
            }
            if (context->debug) printf("Confirmation number: %d\n", command_long_data->confirmation);
        }
        break;
    default:
        if (context->debug) printf("Unknown message type: %d\n", msg->msgid);
        break;
    }
}

static void* qgc_thread_function(__attribute__((unused)) void *vargp) {
    struct sockaddr_in qgc_recvaddr;
    int bytes_read = 0;
    socklen_t addrlen = sizeof(qgc_recvaddr);
    unsigned char write_data_buffer[UDP_READ_BUF_LEN];
    mavlink_message_t msg;
    mavlink_status_t status;
    unsigned int rx_drop_count = 0;
    int msg_received = 0;

    memset(&qgc_recvaddr, 0, sizeof(qgc_recvaddr));

    while (context->running) {
        // Receive UDP message from ground control
        bytes_read = recvfrom(qgc_sockfd, (char*) write_data_buffer,
                              UDP_READ_BUF_LEN, MSG_WAITALL,
                              (struct sockaddr*) &qgc_recvaddr, &addrlen);

        if (bytes_read > 0) {
            if (context->debug) printf("read %d bytes from QGC UDP port\n", bytes_read);

            int i = 0;
            // do the mavlink byte-by-byte parsing
            for (; i < bytes_read; i++) {
                msg_received = mavlink_parse_char(0, write_data_buffer[i], &msg, &status);

                // check for dropped packets
                if (status.packet_rx_drop_count != rx_drop_count) {
                    fprintf(stderr,"DROPPED %d PACKETS\n", status.packet_rx_drop_count);
                }
                rx_drop_count = status.packet_rx_drop_count;

                // Send valid messages to the message handler
                if (msg_received) qgc_message_handler(&msg);
            }
            if (context->debug) {
                if ((i == bytes_read) && ( ! msg_received)) {
                    printf("Looked at all bytes and no valid message\n");
                }
            }
        }
    }

    printf("exiting qgc_thread_function\n");
    return NULL;
}

static void* px4_thread_function(__attribute__((unused)) void *vargp) {
    struct sockaddr_in px4_recvaddr;
    int bytes_read = 0;
    socklen_t addrlen = sizeof(px4_recvaddr);
    unsigned char write_data_buffer[UDP_READ_BUF_LEN];
    mavlink_message_t msg;
    mavlink_status_t status;
    unsigned int rx_drop_count = 0;
    int msg_received = 0;

    memset(&px4_recvaddr, 0, sizeof(px4_recvaddr));

    while (context->running) {
        // Receive UDP message from ground control
        bytes_read = recvfrom(px4_sockfd, (char*) write_data_buffer,
                              UDP_READ_BUF_LEN, MSG_WAITALL,
                              (struct sockaddr*) &px4_recvaddr, &addrlen);

        if (bytes_read > 0) {
            if (context->debug) printf("read %d bytes from QGC UDP port\n", bytes_read);

            int i = 0;
            // do the mavlink byte-by-byte parsing
            for (; i < bytes_read; i++) {
                msg_received = mavlink_parse_char(0, write_data_buffer[i], &msg, &status);

                // check for dropped packets
                if (status.packet_rx_drop_count != rx_drop_count) {
                    fprintf(stderr,"DROPPED %d PACKETS\n", status.packet_rx_drop_count);
                }
                rx_drop_count = status.packet_rx_drop_count;

                // Send valid messages to the message handler
                if (msg_received){
                    printf("Got message from PX4\n");
                }
            }
            if (context->debug) {
                if ((i == bytes_read) && ( ! msg_received)) {
                    printf("Looked at all bytes and no valid message\n");
                }
            }
        }
    }

    printf("exiting px4_thread_function\n");
    return NULL;
}

int qgc_init(context_data *ctx) {
    mavlink_message_t send_message;

    if (ctx == NULL) {
        fprintf(stderr, "ERROR: Null context pointer in qgc_init\n");
        return -1;
    }

    context = ctx;

    if (context->camera_id < MAX_MAVLINK_CAMERAS) {
        context->camera_component_id = MAV_COMP_ID_CAMERA + context->camera_id;
    } else {
        fprintf(stderr, "ERROR: Camera ID must be between 0 and 5\n");
        return PROGRAM_ERROR;
    }

    // Creating socket file descriptor for ground control connection
    if ((qgc_sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) {
        fprintf(stderr, "ERROR: QGC socket creation failed\n");
        return PROGRAM_ERROR;
    }

    if ((px4_sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
        fprintf(stderr, "ERROR: PX4 socket creation failed\n");
        return PROGRAM_ERROR;
    }

    // Now set up our QGC endpoint address so that we can send messages
    memset(&qgc_servaddr, 0, sizeof(qgc_servaddr));
    qgc_servaddr.sin_family = AF_INET; // IPv4
    qgc_servaddr.sin_addr.s_addr = inet_addr(context->qgc_ip_address);
    qgc_servaddr.sin_port = htons(14550);

    memset(&px4_servaddr, 0, sizeof(px4_servaddr));
    px4_servaddr.sin_family = AF_INET; // IPv4
    px4_servaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
    px4_servaddr.sin_port = htons(14551);

    // Pack up the heartbeat message. It never changes.
    mavlink_msg_heartbeat_pack(mavlink_system_id, context->camera_component_id,
                               &send_message, MAV_TYPE_CAMERA,
                               MAV_AUTOPILOT_INVALID, 0, 0, MAV_STATE_ACTIVE);
    camera_heartbeat_message_size = mavlink_msg_to_send_buffer(camera_heartbeat_send_buffer,
                                                               (const mavlink_message_t *) &send_message);

    // Start the thread to receive and process incoming GCS messages.
    pthread_create(&qgc_thread_id, NULL, qgc_thread_function, NULL);
    pthread_create(&px4_thread_id, NULL, px4_thread_function, NULL);

    return 0;
}

void qgc_shutdown() {
    if (context) {
        if (context->debug) printf("Waiting for the threads to exit\n");
        pthread_join(qgc_thread_id, NULL);
        pthread_join(px4_thread_id, NULL);
    }
}
