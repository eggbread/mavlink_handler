
#include <stdio.h>
#include <getopt.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>

#include "context.h"
#include "qgc.h"

// This is the main data structure for the application. It is passed / shared
// with other modules as needed.
static context_data context;

// Used to capture ctrl-c signal to allow graceful exit
void int_handler(int dummy) {
    printf("Got SIGINT, exiting\n");
    context.running = 0;
}

static void print_help() {
    printf("Usage: mavlink-camera-manager <options>\n");
    printf("Options:\n");
    printf("-h     print this help message\n");
    printf("-d     Enable debug messages\n");
    printf("-g     GCS IP address\n");
    printf("\n");
}

int main(int argc, char *argv[]) {
    int extra_options = 0;
    int command_line_option = 0;

    while ((command_line_option = getopt(argc, argv, ":hdg:")) != -1) {
        switch (command_line_option) {
        case 'h':
            print_help();
            return 0;
        case 'd':
            context.debug = 1;
            break;
        case 'g':
            printf("Using QGC IP address: %s\n", optarg);
            strncpy(context.qgc_ip_address, optarg, QGC_IP_ADDRESS_MAX_SIZE);
            break;
        case ':':
            fprintf(stderr, "Error: Option needs a value\n");
            print_help();
            return 0;
        case '?':
            printf("unknown option: %c\n", optopt);
            print_help();
            return 0;
        }
    }

    for (; optind < argc; optind++) {
        fprintf(stderr, "Error: Extra argument: %s\n", argv[optind]);
        extra_options = 1;
    }
    if (extra_options) {
        print_help();
        return PROGRAM_ERROR;
    }

    // Some sanity checking on the command line options
    int qgc_ip_address_length = strlen(context.qgc_ip_address);
    if ( ! qgc_ip_address_length) {
        fprintf(stderr, "Error, Must supply a QGC IP address. Set with the -g option\n");
        print_help();
        return PROGRAM_ERROR;
    }
    if ((qgc_ip_address_length < 7) || (qgc_ip_address_length > 15)) {
        fprintf(stderr, "Error, Invalid QGC IP address %s. Set with the -g option\n",
                context.qgc_ip_address);
        print_help();
        return PROGRAM_ERROR;
    }

    if (context.debug) {
        printf("Parameter summary:\n");
        printf("\tQGC IP address: %s\n", context.qgc_ip_address);
        printf("\tRTSP URI: %s\n", context.rtsp_server_uri);
        printf("\tCamera ID: %d\n", context.camera_id);
        printf("\tCamera name: %s\n", context.camera_name);
    }

    context.running = 1;

    // Setup our signal handler to catch ctrl-c
    signal(SIGINT, int_handler);

    if (qgc_init(&context) == PROGRAM_ERROR) {
        fprintf(stderr, "QGC init failed\n");
        return PROGRAM_ERROR;
    } else {
        if (context.debug) printf("QGC interface started\n");
    }

    // Idle loop. The real work is done in the QGC module threads.
    while (context.running) sleep(1);

    (void) qgc_shutdown();

    return 0;
}

void get_heartbeat() {
    printf("Heartbeat message at camera\n");
}